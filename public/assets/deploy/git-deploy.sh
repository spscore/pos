#!/bin/bash

#accepts 4 agrs: 1 - gir work directory 2 - deployed branch 3 - repo directory 4 - files deployed; 1 and 3 can not be empty

if [[ $# -ne 4 ]]
then 
echo "invalid args"+$#+"\n" >> deploy.log
exit 1
fi
currdir=`pwd`
workingDir=$1
branch=$2
rep=$3
files=$4
now=`date +%Y-%m-%d:%H:%M:%S`

cd $rep
git fetch

git --work-tree=$workingDir checkout -f master
commitHash=`git rev-parse --short HEAD`

cd $currdir
echo "$now Deployed branch: $branch Commit: $commitHash" >> deploy.log

if [[ files -ne '' ]]
then
echo "Files: $files\n\n" >> deploy.log
fi