// Require default library
const isDev = require("electron-is-dev");
const electron = require('electron');
const path = require('path');
const app = electron.app;

// Add custom plugins path
switch (process.platform) {
    case "win32":

        break;
}
// Run electron app
app.on("ready", () => {
    let mainWindow = new electron.BrowserWindow({
        allowRunningInsecureContent: true,
        webPreferences: {
            nodeIntegration: true,
            webSecurity: false
        },
        show: false
    });
    mainWindow.maximize();
    mainWindow.loadURL(!isDev
        ? `file://${path.join(__dirname, "../build/index.html")}`
        : "http://localhost:3000"
    );
    if (isDev) {
        mainWindow.toggleDevTools();
    }
    mainWindow.show();
});

// Close electron
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});