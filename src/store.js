import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import reducers from "./reducers";
import thunk from "redux-thunk";

const persistedState = sessionStorage.getItem("appstate")
    ? JSON.parse(sessionStorage.getItem("appstate"))
    : {};

const configureStore = () => {
    const middlewares = [];

    middlewares.push(thunk);

    if (process.env.REACT_APP_ACC_BOOK !== "production") {
        middlewares.push(createLogger());
    }

    return createStore(reducers, persistedState, applyMiddleware(...middlewares));
};

const store = configureStore();

store.subscribe(() => {
    sessionStorage.setItem("appstate", JSON.stringify(store.getState()));
});

export default store;