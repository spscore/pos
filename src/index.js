import {BrowserRouter, Route} from "react-router-dom";
import {Provider} from "react-redux";
import App from './components/App';
import ReactDOM from 'react-dom';
import store from "./store";
import React from 'react';
import 'bootstrap';

// Init database
// window.database = window.openDatabase('spsystem', '1.0', 'SPSystem', -1);

// Run react
ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <Route component={App}/>
        </Provider>
    </BrowserRouter>,
    document.getElementById('app')
);
