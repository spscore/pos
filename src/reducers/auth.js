import { USER_LOGGED_IN } from "../constants";
import update from "immutability-helper";

export default function user(state = {}, action = {}) {
    switch (action.type) {
        case USER_LOGGED_IN:
            return update(state, {
                tokens: { $set: action.authInfo }
            });
        default:
            return state;
    }
}