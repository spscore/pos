import {combineReducers} from "redux";
import {USER_LOGGED_OUT} from "../constants";

import auth from "./auth";

const appReducer = combineReducers({
    auth,
});

const rootReducer = (state, action) => {
    if (action.type === USER_LOGGED_OUT) {
        return {};
    }
    return appReducer(state, action);
};

export default rootReducer;