import React from "react";

const Container = props => (
    <div>
        <div>{props.children}</div>
    </div>
);

export default (Container);