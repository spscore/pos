import React from "react";

const Navbar = props => (
    <div className="bg-dark navbar-panel pt-2 pb-2">
        <div className="d-flex flex-wrap align-items-center">
            <div className="col-5 pl-0">
                <ul className="nav nav-left col-12">
                    <li>
                        <button className="nav-link">
                            <b>SPS</b>ystem - Термінал
                        </button>
                    </li>
                    <li>
                        <div className="btn-group">
                            <button className="nav-link" data-toggle="dropdown"
                                    aria-expanded="false">
                                <i className="fas fa-bars"/>
                            </button>
                            <div className="dropdown-menu dropdown-menu-right">
                                <a className="dropdown-item" href="#">
                                    <i className="fad fa-user"/>
                                    Профіль
                                </a>
                                <div className="dropdown-divider"/>
                                <a className="dropdown-item" href="#">
                                    <i className="fad fa-sign-out-alt"/>
                                    Вийти
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div className="col-2">
                <ul className="nav nav-center col-12">
                    <li>
                        <button type="button" className="btn btn-default">
                            Інший чек
                        </button>
                    </li>
                </ul>
            </div>
            <div className="col-5 pr-0">
                <ul className="nav nav-right col-12">
                    <li>
                        <button className="nav-link" data-toggle="modal" data-target="#settingsModal">
                            <i className="far fa-cog"/>
                        </button>
                    </li>
                    <li>
                        <div className="btn-group">
                            <button className="nav-link" data-toggle="dropdown"
                                    aria-expanded="false">
                                <i className="fas fa-user"/>
                            </button>
                            <div className="dropdown-menu dropdown-menu-right">
                                <a className="dropdown-item" href="#">
                                    <i className="fad fa-user"/>
                                    Профіль
                                </a>
                                <div className="dropdown-divider"/>
                                <a className="dropdown-item" href="#">
                                    <i className="fad fa-sign-out-alt"/>
                                    Вийти
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div className="modal fade" id="settingsModal" tabIndex="-1"
             aria-labelledby="settingsModalTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="settingsModalTitle">Функції</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">

                        <div className="card">
                            <div className="card-header">Фіскалізація</div>
                            <div className="card-body row row-cols-1 row-cols-md-3">
                                <div className="col">
                                    <div className="card">
                                        <div className="card-body">
                                            X-звіт
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="card">
                                        <div className="card-body">
                                            Z-звіт
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default (Navbar);