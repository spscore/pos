import {withRouter} from "react-router";
import Navbar from "../controls/Navbar";
import api from "../../api";
import React from "react";


class pos extends React.Component {
    state = {
        default_category: 0,
        categories: [],
        products: [],
        items: [],
        total: 0
    };

    constructor(props) {
        super(props);

        // Init pos
        this.getCategories();
    }

    scrollCategories = (e) => {
        // Default params
        try {
            let direction = e.target.getAttribute('data-direction')
            let countElements = (e.target.parentElement.childElementCount - 1);

            let scrollElement = e.target.parentElement.childNodes[countElements];
            if (typeof scrollElement.scrollTop === "number") {
                if (direction === "down") {
                    scrollElement.scrollTop += 60;
                    return true;
                }
                scrollElement.scrollTop -= 60;
                return true;
            }
        } catch (e) {
            console.log(e);
        }
    }

    getCategories = async () => {
        let categories = await api.pos.categories()
        if (typeof categories === "object") {
            this.setState(
                categories
            );
            await this.setCategory(null, this.state.default_category);
        }
    }

    setCategory = async (e, idCategory) => {
        if (typeof idCategory === "undefined") {
            idCategory = e.target.getAttribute('data-id');
        }
        let getProducts = await api.pos.products(idCategory);
        if (typeof getProducts === "object") {
            this.setState({
                products: getProducts
            });
        }
    }

    categories = () => {
        if (Object.keys(this.state.categories).length) {
            return this.state.categories.map((category) =>
                <button className="btn btn-light w-100"
                        onClick={this.setCategory}
                        data-id={category.id}
                        key={category.id}
                        type="button"
                >
                    {category.name}
                </button>
            );
        }
    }

    products() {
        if (Object.keys(this.state.products).length) {
            return this.state.products.map((product) =>
                <div className="col" key={product.id}
                     data-toggle="tooltip"
                     title={product.name}
                >
                    <div onClick={() => this.addCart(product)}
                         className="card card-product">
                        <div className="card-body text-center">
                            <p className="card-text product-title">
                                {product.name}
                            </p>
                            <p className="card-text product-price">
                                {this.formatDecimal(product.price)} ₴
                            </p>
                        </div>
                    </div>
                </div>
            );
        }
    }

    findCartById(key, needle) {
        let result = {};
        for (let i = 0; i < this.state.items.length; i++) {
            if (this.state.items[i][key] === needle) {
                result = {
                    "item": this.state.items[i],
                    "index": i
                };
            }
        }
        return result;
    }

    suspendCart() {

        this.clearCart();
    }

    removeCart(id) {
        let itemsTmp = this.state.items;
        let total = 0;

        let itemExist = this.findCartById("id", id);

        if (Object.keys(itemExist).length > 0) {
            itemsTmp.splice(itemExist.index, 1);
        }

        // Calc Cart
        itemsTmp.forEach(function (value) {
            total += value.total;
        });

        // Set state
        this.setState({
            items: itemsTmp,
            total: total
        });
    }

    clearCart() {
        this.setState({
            items: [],
            total: 0
        });
    }


    addCart(product) {
        let itemsTmp = this.state.items;
        let calcQuantity = 1;
        let total = 0;

        let itemExist = this.findCartById("id", product.id);

        if (Object.keys(itemExist).length > 0) {
            // Calculate updates
            calcQuantity = itemExist.item.quantity + calcQuantity;

            // Update data
            itemsTmp[itemExist.index]['total'] = (calcQuantity * product.price);
            itemsTmp[itemExist.index]['quantity'] = calcQuantity;
        } else {
            itemsTmp.unshift({
                "total": (calcQuantity * product.price),
                "quantity": calcQuantity,
                "cssClass": "",//"highlight",
                "price": product.price,
                "name": product.name,
                "id": product.id,
                "row": {}
            });
        }

        // Calc Cart
        itemsTmp.forEach(function (value) {
            total += value.total;
        });

        // Set state
        this.setState({
            items: itemsTmp,
            total: total
        });

        // Calc Cart
        itemsTmp.forEach(function (value) {
            total += value.total;
        });

        // Set state
        this.setState({
            items: itemsTmp,
            total: total
        });
    }

    cart() {
        if (Object.keys(this.state.items).length > 0) {
            return this.state.items.map((item) =>
                <tr data-product-id={item.id} key={item.id} className={item.cssClass}>
                    <td className="name">
                        <span>{item.name}</span>
                    </td>

                    <td className="count">
                        <input className="form-control text-center" type="text" readOnly value={item.quantity}/>
                    </td>

                    <td className="price">{this.formatDecimal(item.price)}</td>
                    <td className="total">
                        {this.formatDecimal(item.total)}
                    </td>
                    <td className="trash remove" onClick={() => this.removeCart(item.id)}>
                        <i className="far fa-times"/>
                    </td>
                </tr>
            );
        }
    }

    formatDecimal(x, fixed) {
        let minFix = 2;
        if (fixed) {
            minFix = fixed;
        }
        return parseFloat(x).toFixed(minFix);
    }

    render() {
        return (
            <React.Fragment>
                <Navbar/>
                <div className="pos-content-wrapper row">
                    <div className="panel-left col-4 p-1">
                        <div className="products">
                            <div className="list">
                                <table className="table table-striped table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th className="name">Найменування</th>
                                        <th className="count">Кіл-ть</th>
                                        <th className="price">Ціна</th>
                                        <th className="total">Разом</th>
                                        <th className="trash">
                                            <i className="far fa-trash"/>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.cart()}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="summary">
                            <dl className="total">
                                <dt>До сплати</dt>
                                <dd>{this.formatDecimal(this.state.total)} ₴</dd>
                            </dl>
                            <div className="buttons row">
                                <div className="action col-6">
                                    <div className="btn-group dropup">
                                        <button type="button" className="btn btn-lg btn-outline-primary dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <i className="fas fa-ellipsis-h"/>
                                        </button>
                                        <div className="dropdown-menu">
                                            <button className="dropdown-item" type="button"
                                                    onClick={() => this.suspendCart()}>
                                                Призупинити замовлення
                                            </button>
                                            <div className="dropdown-divider"/>
                                            <button className="dropdown-item" type="button"
                                                    onClick={() => this.clearCart()}>
                                                Очистити замовлення
                                            </button>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-outline-secondary" data-toggle="modal"
                                            data-target="#checkListModal">
                                        <i className="fad fa-shopping-basket"/>
                                    </button>
                                </div>
                                <div className="col-6">
                                    <button className="btn btn-lg btn-success w-100"
                                            data-target="#payModal"
                                            data-toggle="modal"
                                    >
                                        Сплатити
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="panel-center col-6 pb-1">
                        <div className="row row-cols-1 row-cols-xl-4 row-cols-lg-3 row-cols-md-2 g-3 products-list">
                            {this.products()}
                        </div>
                    </div>
                    <div className="panel-right col-2 p-1">
                        <div className="col-12 text-center h-100 p-0 categories">
                            <button className="btn btn-primary w-100 arrow-up"
                                    onClick={this.scrollCategories}
                                    data-direction="up"
                                    type="button"
                            >
                                <i className="fas fa-arrow-up"/>
                            </button>
                            <button className="btn btn-primary w-100 arrow-down"
                                    onClick={this.scrollCategories}
                                    data-direction="down"
                                    type="button"
                            >
                                <i className="fas fa-arrow-down"/>
                            </button>
                            <div id="categories-list">
                                {this.categories()}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="payModal" tabIndex="-1"
                     aria-labelledby="payModalTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-xl">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="payModalTitle">Вікно оплати</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <h4>До сплати: {this.formatDecimal(this.state.total)} ₴</h4>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">
                                    Закрити
                                </button>
                                <button type="button" className="btn btn-success">
                                    Сплатити
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="checkListModal" tabIndex="-1"
                     aria-labelledby="checkListTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="checkListTitle">Modal title</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p>Placeholder text for this demonstration of a vertically centered modal dialog.</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(
    pos
);