import { USER_LOGGED_IN, USER_LOGGED_OUT } from "../../constants";
import { setAuthorizationHeader } from "../../utils";
import api from "../../api";

const userLoggedIn = authInfo => ({
    type: USER_LOGGED_IN,
    authInfo
});

const userLoggedOut = () => ({
    type: USER_LOGGED_OUT
});

export const loginUser = (credentials) => async dispatch => {
    const response = await api.customer.login(credentials);
    console.log(response);
    setAuthorizationHeader(response.token);
    dispatch(userLoggedIn(response));
};

export const logout = () => dispatch => {
    sessionStorage.removeItem("appstate");
    sessionStorage.removeItem("token");
    setAuthorizationHeader();
    dispatch(userLoggedOut());
};