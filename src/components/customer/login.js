import {withRouter} from "react-router";
import {connect} from "react-redux";
import {loginUser} from "./actions";
import React from "react";

class login extends React.Component {
    state = {
        data: {
            identity: ""
        },
        loading: false,
        errors: null
    };

    constructor(props) {
        super(props);
        this.usernameRef = null;
    }

    UNSAFE_componentWillMount() {
        if (this.props.isLoggedIn) {
            this.props.history.push("/home");
        } else {
            this.props.history.push("/");
        }
    }

    setUsernameRef = element => {
        this.usernameRef = element;
    };

    onChange = e => {
        this.setState({
            data: {...this.state.data, [e.target.name]: e.target.value}
        });
    };

    onSubmit = async (e) => {
        console.log(this.state);
        e.preventDefault();
        if (this.usernameRef) this.usernameRef.focus();

        try {
            await this.props.loginUser({...this.state.data});
            this.props.history.push("/");
        } catch (error) {
            this.setState({
                error: error.message,
                data: {identity: ""}
            });

            if (this.usernameRef) this.usernameRef.focus();
        }
    }

    render() {
        return (
            <div className="login-page">
                <div className="text-center">
                    <main className="form-signin">
                        <form className="needs-validation" onSubmit={this.onSubmit}>
                            <img className="mb-4" src="assets/media/app-logo.png" alt=""/>
                            <div className="form-floating">
                                <input
                                    className={"form-control " + (this.state.error ? "is-invalid" : "")}
                                    value={this.state.data.identity}
                                    ref={this.setUsernameRef}
                                    onChange={this.onChange}
                                    placeholder="Пароль"
                                    name="identity"
                                    type="password"
                                />
                                <div className="invalid-feedback">
                                    {this.state.error}
                                </div>
                            </div>
                            <button className="w-100 btn btn-success" type="submit">Увійти</button>
                        </form>
                    </main>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const isLoggedIn = state.auth !== undefined ? !!state.auth.tokens : false;

    return {
        isLoggedIn
    };
}

export default withRouter(
    connect(
        mapStateToProps,
        {loginUser}
    )(login)
);