// Import components
import LoginPage from "./customer/login";
import {withRouter} from "react-router";
import React, {Fragment} from "react";
import {connect} from "react-redux";
import Home from "./terminal/pos";

// Import styles
import '../assets/scss/fontawesome-all.scss';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/scss/application.scss';

const App = props => {
    const checkForAuth = () => {
        if (props.isLoggedIn) {
            return <Home/>;
        }
        return <LoginPage/>;
    };
    return <Fragment>{checkForAuth()}</Fragment>;
};

function mapStateToProps(state) {
    const isLoggedIn = state.auth !== undefined ? !!state.auth.tokens : false;
    return {
        isLoggedIn
    };
}

export default withRouter(connect(mapStateToProps, null)(App));
