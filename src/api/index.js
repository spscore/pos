// Set default components
import apiBuilder, {errorHandler, URL_PREFIX} from "./apiBuilder";
import axios from "axios";
import "./axiosClient";

// Register rest methods
const customer = apiBuilder("customers");
customer.login = async ({identity}) => {
    try {
        const resource = await axios.post(`${URL_PREFIX}/customer/login?_=` + Math.random(), {
            "identity": identity
        });
        let response = resource.data;
        if (response.status === false) {
            throw new Error(response.error);
        }
        return response.message;
    } catch (error) {
        if (error.response && error.response.status === 500) {
            throw new Error("Server error, please try again after sometime.");
        }
        throw error;
    }
};

const pos = apiBuilder("pos");
pos.categories = async () => {
    try {
        let resource = await axios.get(`${URL_PREFIX}/pos/categories`);
        let response = resource.data;
        if (response.status === false) {
            throw new Error(response.error);
        }
        return response.message;
    } catch (error) {
        errorHandler(error);
        return false;
    }
}
pos.products = async (category) => {
    try {
        let resource = await axios.get(`${URL_PREFIX}/pos/products?category_id=${category}`);
        let response = resource.data;
        if (response.status === false) {
            throw new Error(response.error);
        }
        return response.message;
    } catch (error) {
        errorHandler(error);
        return false;
    }
}

// eslint-disable-next-line
export default {
    customer,
    pos
};