import {setAuthorizationHeader} from "../utils";
import axios from "axios";

// Set token
setAuthorizationHeader(sessionStorage.getItem("token"));

// Add a response interceptor
axios.interceptors.response.use(
    response => response,
    error => {
        if (error.response) {
            const {data} = error.response;

            if (data.message) {
                return Promise.reject(new Error(data.message));
            }
        }
        return Promise.reject(error);
    }
);