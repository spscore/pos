import axios from "axios";

export const URL_PREFIX = process.env.NODE_ENV === "development" ?
    "https://try.spsystem.pro" :
    "https://api.spsystem.pro";

export const logout = () => {
    sessionStorage.removeItem("appstate");
    sessionStorage.removeItem("token");
    window.location.reload();
}


// Custom functional
export const errorHandler = (error) => {
    if (error.response && error.response.status === 500) {
        throw new Error("Server error, please try again after sometime.");
    }
    
    if (error.response && error.response.status === 401) {
        logout();
    }
    console.log('Catch ', error);
}

const apiBuilder = mainRouteName => ({
    fetchAll: async () => {
        try {
            let resource = await axios.get(`${URL_PREFIX}/${mainRouteName}/`);
            let response = resource.data;
            if (response.status === false) {
                throw new Error(response.error);
            }
            return response.message;
        } catch (e) {
            console.log('Catch: ', e.message);
            return false;
        }
    },

    fetchByPages: () => axios.get(`${URL_PREFIX}/${mainRouteName}?per_page=10&page=1`),

    searchByIdAndGetByPages: (id) => axios.get(`${URL_PREFIX}/${mainRouteName}?q=${id}&per_page=10&page=1`),

    searchByIdAndGetAll: id => axios.get(`${URL_PREFIX}/${mainRouteName}/all?q=${id}`),

    createNew: item => axios.post(`${URL_PREFIX}/${mainRouteName}`, item),

    update: (id, item) => axios.put(`${URL_PREFIX}/${mainRouteName}/${id}`, item),

    delete: id => axios.delete(`${URL_PREFIX}/${mainRouteName}/${id}`)
});

export default apiBuilder;
